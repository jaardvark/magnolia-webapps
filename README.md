# README #

Ready-to-run maven WAR projects for magnolia cms (see http://www.magnolia-cms.com).

### Magnolia-CMS out of the box ###

* Magnolia CMS Maven WAR projects
* Overlay magnolia cms WARs ("magnolia bundle") with ready-to-run development configuration
* H2 Database used for repository
* Author instance and public instance
* Ready to run in tomcat and eclipse IDE
* Groovy, Webdav, SCSS and Synchronization modules added

### How to use ###

Basic setup:

* Install current eclipse (Luna or later)
* Clone repository
* Add projects to eclipse
* Define tomcat server
* Add web projects to server
* Run

Once you are up and running, you can start developing your own module:

* Create a module project (see documentation on magnolia-cms.com)
* Add the dependency for your module project to the web-app project's pom.xml
* Run, and see your module in action!

Note that you don't have to use eclipse. You can use the IDE of your choice, or plain maven.

### Contributions ###

Feel free to fork this and use for your own development.
If you have enhancements, feel free to send me a pull request.

### Who do I talk to? ###

Richard Unger, feel free to contact me via BitBucket